import { Checkbox, FormControlLabel, Typography, Box } from '@mui/material'
import TPasswordOptions from '../type/TPasswordOptions'
import CoreSlider from '../commonComponents/CoreSlider'

interface IFilterPasswordProps {
    setOptions: React.Dispatch<React.SetStateAction<TPasswordOptions>>
    options: TPasswordOptions
}

const FilteredPassword: React.FC<IFilterPasswordProps> = ({
    options,
    setOptions,
}) => {
    const handleOptionChange = (
        option: keyof TPasswordOptions,
        value: boolean | number
    ): void => {
        // Update the options state based on the changed option
        setOptions((prevOptions) => ({
            ...prevOptions,
            [option]: value,
        }))
    }

    return (
        <Box p={2}>
            {/* Slider for update password length */}

            <Typography>Password length :{options.passwordLength} </Typography>
            <CoreSlider
                handleOptionChange={handleOptionChange}
                options={options}
            />
            {/* Checkboxes for various password options */}
            {Object.entries({
                includeUpperCase: 'Uppercase',
                includeLowerCase: 'Lowercase',
                includeNumbers: 'Numbers',
                includeSpecialChars: 'Special Characters',
                avoidAmbiguous: 'Easy to Read (Avoid 0, O, I, l)',
            }).map(([option, label]) => (
                <FormControlLabel
                    key={option}
                    control={
                        <Checkbox
                            checked={
                                !!options[option as keyof TPasswordOptions]
                            }
                            onChange={(e) =>
                                handleOptionChange(
                                    option as keyof TPasswordOptions,
                                    e.target.checked
                                )
                            }
                        />
                    }
                    label={label}
                />
            ))}
        </Box>
    )
}

export default FilteredPassword
