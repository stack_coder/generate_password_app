import React, { useEffect, useState } from 'react'
import CachedIcon from '@mui/icons-material/Cached'
import { passwordStrength } from 'check-password-strength'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import { FileCopy as ClipboardIcon } from '@mui/icons-material'
import copyToClipboard from '../helperFunction/copyToClipboard'
import { Box, Typography } from '@mui/material'
import CoreButton from '../commonComponents/CoreButton'
import TPasswordOptions from '../type/TPasswordOptions'
import generateRandomPassword from '../helperFunction/generateRandomPassword'
import FilteredPassword from './FilteredPassword'

const PasswordGenerator: React.FC = () => {
    const [options, setOptions] = useState<TPasswordOptions>({
        // Default options for password generation
        passwordLength: 12, // Default password length
        includeUpperCase: true, // Include uppercase letters by default
        includeLowerCase: true, // Include lowercase letters by default
        includeNumbers: true, // Include numbers by default
        includeSpecialChars: true, // Include special characters by default
        avoidAmbiguous: true, // Avoid ambiguous characters by default
    })

    const [generatedPassword, setGeneratedPassword] = useState('') // State to store the generated password

    const [passwordStrengthColor, setPasswordStrengthColor] = useState('') // State to store the color representing password strength

    const [message, setMessage] = useState('copy') // State to manage message for copying status

    const generatePassword = (): void => {
        // Generate a random password based on the provided options
        const password = generateRandomPassword(options)
        // Set the generated password state
        setGeneratedPassword(password)

        try {
            // Calculate the strength of the generated password
            const strengthResult = passwordStrength(password)
            const strength = strengthResult.value
            // Set color based on password strength
            setPasswordStrengthColor(
                strength === 'Strong'
                    ? 'green'
                    : strength === 'Medium'
                      ? 'orange'
                      : 'red'
            )
        } catch (error) {
            // Handle errors when calculating password strength
            console.error('Failed to calculate password strength:', error)
        }
    }

    useEffect(() => generatePassword(), [options])

    return (
        <Box mt={2} display="flex" flexDirection="column" alignItems="center">
            {/* Display generated password */}
            <Typography
                variant="h4"
                style={{
                    marginBottom: '28px',
                    overflowWrap: 'break-word',
                    wordWrap: 'break-word',
                    wordBreak: 'break-word',
                }}
            >
                {generatedPassword}
            </Typography>
            {/* Display strength message */}
            <Typography
                variant="h5"
                style={{ marginBottom: '30px', color: passwordStrengthColor }}
            >
                {passwordStrengthColor === 'green'
                    ? 'Strong'
                    : passwordStrengthColor === 'orange'
                      ? 'Medium'
                      : 'Weak'}
            </Typography>
            <Box display="flex" justifyContent="center">
                <CoreButton
                    icon={<CachedIcon />} // Pass the CachedIcon component as the icon prop
                    variant="contained"
                    onClick={generatePassword}
                />
                <Box ml={2}>
                    <CoreButton
                        variant="contained"
                        text={message}
                        icon={
                            message === 'copy' ? (
                                <ContentCopyIcon />
                            ) : (
                                <ClipboardIcon />
                            )
                        } // Pass the ClipboardIcon component as the icon prop
                        onClick={() =>
                            copyToClipboard(generatedPassword, setMessage)
                        }
                    />
                </Box>
            </Box>
            {/* Filtered the generated password according to user Input */}
            <FilteredPassword setOptions={setOptions} options={options} />
        </Box>
    )
}

export default PasswordGenerator
