import { Box } from '@mui/material'
import PasswordGenerator from './component/PasswordGenerator'

const App: React.FC = () => {
    const containerStyle: React.CSSProperties = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100vh', // Changed height to minHeight for better responsiveness
        backgroundColor: '#1e203e',
        padding: '20px', // Added padding for better spacing
    }

    const boxContainerStyle: React.CSSProperties = {
        width: '90%', // Adjusted width for smaller screens
        maxWidth: '500px', // Added maximum width for larger screens
        border: '5px solid #1c49c0',
        display: 'flex',
        borderRadius: '10px',
        flexDirection: 'column',
    }

    const firstRowStyle: React.CSSProperties = {
        flex: '30%',
        backgroundColor: 'white',
        padding: '20px', // Added padding for better spacing
        borderRadius: '10px 10px 0 0', // Adjusted border radius for top corners
    }

    const secondRowStyle: React.CSSProperties = {
        flex: '70%',
        backgroundColor: 'white',
        padding: '20px', // Added padding for better spacing
        borderRadius: '0 0 10px 10px', // Adjusted border radius for bottom corners
    }
    const headingStyle: React.CSSProperties = {
        textAlign: 'center',
        margin: '0', // Removed default margin for better alignment
    }

    return (
        <div style={containerStyle}>
            <Box style={boxContainerStyle}>
                <Box style={firstRowStyle}>
                    <h1 style={headingStyle}>PASSWORD GENERATOR</h1>
                    <p style={headingStyle}>
                        Ensure online account safety by creating strong and
                        secure passwords
                    </p>
                </Box>
                <Box style={secondRowStyle}>
                    <PasswordGenerator />
                </Box>
            </Box>
        </div>
    )
}

export default App
