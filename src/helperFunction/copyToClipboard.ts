const copyToClipboard = (
    generatedPassword: string, // The generated password to be copied to clipboard
    setMessage: (message: string) => void // Function to set a message indicating the copy status
): void => {
    // Write the generated password to the clipboard using the Clipboard API
    navigator.clipboard.writeText(generatedPassword)

    // Set a message indicating that the password has been copied
    setMessage('Copied')

    // Reset the message after a certain delay to provide feedback to the user
    setTimeout(() => setMessage('copy'), 1000)
}
export default copyToClipboard
