import TPasswordOptions from '../type/TPasswordOptions'

const generateRandomPassword = (options: TPasswordOptions): string => {
    // Destructure options object to extract relevant properties
    const {
        passwordLength,
        includeUpperCase,
        includeLowerCase,
        includeNumbers,
        includeSpecialChars,
        avoidAmbiguous,
    } = options

    // Construct the character set based on selected options
    const charset = [
        includeUpperCase && 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        includeLowerCase && 'abcdefghijklmnopqrstuvwxyz',
        includeNumbers && '0123456789',
        includeSpecialChars && '!@#$%^&*()_+{}[]|:;"<>,.?/~',
    ]
        // Filter out falsy values (for unchecked options) and join the character sets
        .filter(Boolean)
        .join('')

    // Filter out ambiguous characters if selected
    const filteredCharset = avoidAmbiguous
        ? charset.replace(/[0OIl]/g, '') // Removing 0, O, I, l to avoid ambiguity
        : charset

    // Generate password by randomly selecting characters from the filtered charset
    const password = Array.from({ length: passwordLength }, () =>
        filteredCharset.charAt(
            Math.floor(Math.random() * filteredCharset.length)
        )
    ).join('')

    return password // Return the generated password
}

export default generateRandomPassword
