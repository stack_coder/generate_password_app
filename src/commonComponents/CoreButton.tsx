import React, { ReactNode } from 'react'
import Button, { ButtonOwnProps } from '@mui/material/Button'

interface ICoreButtonProps extends ButtonOwnProps {
    variant?: 'text' | 'outlined' | 'contained'
    text?: string
    icon?: ReactNode
    bgCol?: string
    textCol?: string
    size?: 'small' | 'medium' | 'large'
    buttonHeight?: string
    fontSize?: string
    paddings?: string
    fullWidth?: boolean
    margins?: string
    onClick?: () => void
}

const CoreButton: React.FC<ICoreButtonProps> = ({
    // Default props for the button
    variant = 'outlined', // Button variant (default: outlined)
    icon, // Icon to display on the button
    bgCol, // Background color of the button
    textCol, // Text color of the button
    buttonHeight, // Height of the button
    size, // Size of the button
    text, // Text displayed on the button
    fontSize, // Font size of the button text
    fullWidth, // Whether the button should span full width
    paddings = '12px', // Padding of the button
    margins, // Margin of the button
    onClick, // onClick event handler
}) => {
    return (
        <Button
            variant={variant}
            startIcon={icon}
            style={{
                textTransform: 'none', // prevent text converting to titlecase by default
                backgroundColor: bgCol,
                color: textCol,
                height: buttonHeight,
                fontSize: fontSize,
                padding: paddings,
                borderRadius: '8px',
                marginRight: margins,
            }}
            size={size}
            fullWidth={fullWidth}
            onClick={onClick}
        >
            {text}
        </Button>
    )
}

export default CoreButton
