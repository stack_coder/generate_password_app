import * as React from 'react'
import Box from '@mui/material/Box'
import Slider from '@mui/material/Slider'
import TPasswordOptions from '../type/TPasswordOptions'

interface ICoreSliderProps {
    handleOptionChange: (
        option: keyof TPasswordOptions,
        value: number | boolean
    ) => void
    options: TPasswordOptions
}
const CoreSlider: React.FC<ICoreSliderProps> = ({
    handleOptionChange,
    options,
}) => {
    const [value, setValue] = React.useState(30)

    const handleChange = (event: Event, newValue: number | number[]): void => {
        setValue(newValue as number)
        handleOptionChange('passwordLength', value)
    }
    return (
        <Box sx={{ width: 300 }}>
            <Slider
                value={
                    typeof options.passwordLength === 'number'
                        ? options.passwordLength
                        : 0
                }
                onChange={handleChange}
                aria-labelledby="input-slider"
                max={30}
                min={5}
            />
        </Box>
    )
}

export default CoreSlider
