type TPasswordOptions = {
    passwordLength: number
    includeUpperCase: boolean
    includeLowerCase: boolean
    includeNumbers: boolean
    includeSpecialChars: boolean
    avoidAmbiguous: boolean
}

export default TPasswordOptions
